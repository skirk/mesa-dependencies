

set(arm-extra-flags "")
set(autoconf_toolset_flags "")

if (${CMAKE_SYSTEM_PROCESSOR} MATCHES "arm")
	set(arm-extra-flags --enable-malloc0returnsnull)
	set(autoconf_toolset_flags --host=arm-linux-gnueabihf)
endif()

function(addAutotoolsProjectWithEnvVarsAndExtraFlags project repo tag envVar extraFlags)
	ExternalProject_Add(${project}
		DEPENDS ${ARGN}
		GIT_REPOSITORY ${repo}
		GIT_TAG ${tag}
		UPDATE_COMMAND ""
		CONFIGURE_COMMAND ${envVar} PKG_CONFIG_PATH=${MY_PKG_CONFIG_PATH} autoreconf -I ${MY_AC_EXTRAS_PATH} -v --install
		COMMAND ${envVar} PKG_CONFIG_PATH=${MY_PKG_CONFIG_PATH} ./configure --prefix=${CMAKE_INSTALL_PREFIX} ${autoconf_toolset_flags} ${extraFlags}
		BUILD_COMMAND make -j 32
		INSTALL_COMMAND make install
		BUILD_IN_SOURCE TRUE)
endfunction()

function(addAutotoolsProjectArchive project url)
	ExternalProject_Add(${project}
		DEPENDS ${ARGN}
		URL ${url}
		UPDATE_COMMAND ""
		CONFIGURE_COMMAND PKG_CONFIG_PATH=${MY_PKG_CONFIG_PATH} autoreconf -I ${MY_AC_EXTRAS_PATH} -v --install
		  	  COMMAND PKG_CONFIG_PATH=${MY_PKG_CONFIG_PATH} ./configure --prefix=${CMAKE_INSTALL_PREFIX} ${autoconf_toolset_flags}
		BUILD_COMMAND make -j 32
		INSTALL_COMMAND make install
		BUILD_IN_SOURCE TRUE)
endfunction()

function(addAutotoolsProject project repo tag)
	addAutotoolsProjectWithEnvVarsAndExtraFlags(${ARGV0} ${ARGV1} ${ARGV2} "" "" ${ARGN})
endfunction()

function(addAutotoolsProjectWithEnvVars project repo tag envVars)
	addAutotoolsProjectWithEnvVarsAndExtraFlags(${ARGV0} ${ARGV1} ${ARGV2} ${envVars} "" ${ARGN})
endfunction()

function(addAutotoolsProjectWithExtraFlags project repo tag extraFlags)
	addAutotoolsProjectWithEnvVarsAndExtraFlags(${ARGV0} ${ARGV1} ${ARGV2} "" ${extraFlags} ${ARGN})
endfunction()




